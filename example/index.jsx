import React from 'react'
import ReactDOM from 'react-dom'
import { convertRawToHTML, convertHTMLToRaw } from '../src'

// 未完善

class Demo extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      source: '',
      result: ''
    }
  }

  componentDidMount() {

    const draftRawData = '{"blocks":[{"key":"11s4f","text":"Hello World!baidu.com","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":6,"length":5,"style":"BOLD"},{"offset":6,"length":5,"style":"COLOR-F32784"}],"entityRanges":[{"offset":12,"length":9,"key":0}],"data":{}}],"entityMap":{"0":{"type":"LINK","mutability":"MUTABLE","data":{"href":"http://www.baidu.com/","target":""}}}}'
    const htmlString = '<p class="helloworld" style="text-indent:12em;text-align:center;">Hello <strong><span style="color:#f32784">World</span></strong>!<a href="http://www.baidu.com">baidu.com</a></p>'

    const testHtml = '<p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><div class="media-wrap video-wrap"><video controls="" class="media-wrap video-wrap" poster="http://oxiaoeyu.oss-cn-beijing.aliyuncs.com/9rmG3OGvoe/aodwJaG649/20210403/1617420433563064.jpeg" src="https://cdn.jsdelivr.net/gh/eyux/assets@latest/a/video/coffee11.mp4" width="22px" height="22px"></video></div><p></p><p></p><p>https://cdn.jsdelivr.net/gh/eyux/assets@latest/a/video/coffee11.mp4</p><div class="media-wrap video-wrap"><video controls="" class="media-wrap video-wrap" poster="http://oxiaoeyu.oss-cn-beijing.aliyuncs.com/9rmG3OGvoe/aodwJaG649/20210201/1612117090372537.jpeg" src="https://cdn.jsdelivr.net/gh/eyux/assets@latest/a/video/coffee11.mp4"></video></div><p></p><p>1111111111</p><div class="media-wrap video-wrap"><video controls="" class="media-wrap video-wrap" poster="http://oxiaoeyu.oss-cn-beijing.aliyuncs.com/9rmG3OGvoe/aodwJaG649/20210403/1617420433563064.jpeg" src="https://cdn.jsdelivr.net/gh/eyux/assets@latest/a/video/coffee11.mp4"></video></div><p></p><p></p><div class="media-wrap video-wrap"><video controls="" class="media-wrap video-wrap" src="https://cdn.jsdelivr.net/gh/eyux/assets@latest/a/video/coffee11.mp4"></video></div><p></p><p>2021年2月5日，“短视频第一股”快手登陆香港交易所，发行价115港元/股，股票代码1024.HK，在香港上市首日高开193.91%，报338港元，市值超过1.39万亿港元。中国又一家互联网企业，冲入万亿级别行列。</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">从快手的财务报表可以看到，其收入从2017年的83亿元增至2018年aadadasd的203亿元，再进一步增至2019年的391亿元。2020年上半年营收为407亿元，去年同期为273亿元。</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">如此精彩的表现和华丽的数据，让人不禁想到其竞争对手“抖音”，同时也对抖音的上市越来越期待。“既生快，何生抖？”虽然快手是先行者，但是抖音借助今日头条的资源背景和算法，一度形成超越快手的态势。</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">根据统计数据显示，抖音MAU（月活跃用户人数）已经长期超越了快手，虽然快手一直追赶，但是仍是处于下风。</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">从图中看出，从2018年4月份抖音超过快手之后，抖音就一直处于领先状态。2020年9月快手主站MAU为4.08亿，2020年9月抖音短视频MAU为5.24亿。</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false"></p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">快手：先关系再内容 VS 抖音：先内容再关系</p><p style="text-indent:2em;" size="0" _root="undefined" __ownerid="undefined" __hash="undefined" __altered="false">。</p>'
    // console.log(convertRawToHTML(JSON.parse(draftRawData)))
    // <p>Hello <strong><span style="color:#f32784">World</span></strong>!</p>

    // console.log(JSON.stringify(convertHTMLToRaw(htmlString)))
    console.log(convertRawToHTML(convertHTMLToRaw(testHtml)))
    // {"blocks":[{"key":"8v6eh","text":"Hello World!","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":6,"length":5,"style":"BOLD"},{"offset":6,"length":5,"style":"COLOR-F32784"}],"entityRanges":[],"data":{}}],"entityMap":{}}

  }

  render() {
    return null
  }

}

ReactDOM.render(<Demo />, document.querySelector('#root'))
